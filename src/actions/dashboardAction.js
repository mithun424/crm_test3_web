import axios from "axios";
import * as TYPE from "./action-types";

export const getChartSuccess = (chartData) => {
  return {
    type: TYPE.GET_CHARTDATA_SUCCESS,
    payload: chartData,
  };
};

export const getChartFailure = (err) => {
  return {
    type: TYPE.GET_CHARTDATA_FAILURE,
    payload: {
      message: err.message,
    },
  };
};

export const getChartBegin = () => {
  return {
    type: TYPE.GET_CHARTDATA_BEGIN,
  };
};

export const getChartData = () => {
  return (dispatch, getState) => {
    dispatch(getChartBegin());
    return axios.get("http://localhost:8080/api/comment/chart").then(
      (resp) => {
        const chartData = [];
        let charts = [];
        if (resp.data.chartVMList && resp.data.chartVMList.length > 0) {
          chartData.push(["Status", "Total complaints", { role: "style" }]);
          resp.data.chartVMList.forEach((chart) => {
            charts.push(chart.status);
            charts.push(chart.totalComments);
            charts.push(chart.style);
            chartData.push(charts);
            charts = [];
          });
        }
        dispatch(getChartSuccess(chartData));
        return true;
      },
      (err) => {
        dispatch(getChartFailure(err));
        return false;
      }
    );
  };
};
