import * as TYPE from "./../actions/action-types";
import customerReducer from "./customerReducer";

describe("customerReducer", () => {
    let initialState = { entities: [], loading: false, error: null };
    it("should return initial state", () => {
      const result = customerReducer(undefined, {});
      expect(result).toEqual(initialState);
    });

    it("should return FETCH_CUSTOMERS_BEGIN state", () => {
        const action = {
          type: TYPE.FETCH_CUSTOMERS_BEGIN,
        };
        const result = customerReducer(
            initialState,
          action
        );
        expect(result).toEqual({ entities: [], loading: true, error: null });
      });

      it("should return FETCH_CUSTOMERS_SUCCESS state", () => {
        const action = {
          type: TYPE.FETCH_CUSTOMERS_SUCCESS,
          payload:[{
            id: "42c8150e-6663-4649-a486-175da825a781",
            customerId: "4b1a32da-01d6-4f86-ae8a-8dd68b1d63c3",
            firstName: "Sudham P",
            lastName: "Title 2",
            email: "Desc 23",
            mobile: "OPEN",
            role: "0298b44d-b45a-47af-a09d-ecc9bebd88fb",
          }]
        };
        const result = customerReducer(
            initialState,
          action
        );
        expect(result).toEqual({ ...initialState, entities: action.payload, loading: false });
      });

      it("should return FETCH_CUSTOMERS_FAILURE state", () => {
        const action = {
          type: TYPE.FETCH_CUSTOMERS_FAILURE,
          payload: {
            message: "Failed to fetch cutomer.. please try again later!!!",
          },
        };
        const result = customerReducer(
            initialState,
          action
        );
        expect(result).toEqual({ ...initialState, entities: [], loading: false, error: action.payload });
      });
      it("should return SAVE_UPDATE_CUSTOMER_BEGIN state", () => {
        const action = {
          type: TYPE.SAVE_UPDATE_CUSTOMER_BEGIN,
        };
        const result = customerReducer(
            initialState,
          action
        );
        expect(result).toEqual({ ...initialState, success: false, loading: true });
      });
      it("should return DELETE_CUSTOMER_BEGIN state", () => {
        const action = {
          type: TYPE.DELETE_CUSTOMER_BEGIN,
        };
        const result = customerReducer(
            initialState,
          action
        );
        expect(result).toEqual({ ...initialState, success: false, loading: true });
      });

      it("should return SAVE_UPDATE_CUSTOMER_SUCCESS state", () => {
        const action = {
          type: TYPE.SAVE_UPDATE_CUSTOMER_SUCCESS,
          payload: {
            id: "42c8150e-6663-4649-a486-175da825a781",
            customerId: "4b1a32da-01d6-4f86-ae8a-8dd68b1d63c3",
            firstName: "Sudham P",
            lastName: "Title 2",
            email: "Desc 23",
            mobile: "OPEN",
            role: "0298b44d-b45a-47af-a09d-ecc9bebd88fb",
          }
        };
        const result = customerReducer(
            initialState,
          action
        );
        expect(result).toEqual({ ...initialState, success: true, loading: false });
      });

      it("should return DELETE_CUSTOMER_SUCCESS state", () => {
        const action = {
          type: TYPE.DELETE_CUSTOMER_SUCCESS,
          payload: {
            id: "42c8150e-6663-4649-a486-175da825a781",
            customerId: "4b1a32da-01d6-4f86-ae8a-8dd68b1d63c3",
            firstName: "Sudham P",
            lastName: "Title 2",
            email: "Desc 23",
            mobile: "OPEN",
            role: "0298b44d-b45a-47af-a09d-ecc9bebd88fb",
          }
        };
        const result = customerReducer(
            initialState,
          action
        );
        expect(result).toEqual({ ...initialState, success: true, loading: false });
      });

      it("should return SAVE_UPDATE_CUSTOMER_FAILURE state", () => {
        const action = {
          type: TYPE.SAVE_UPDATE_CUSTOMER_FAILURE,
          payload: { message: "errorMessage" },
        };
        const result = customerReducer(
            initialState,
          action
        );
        expect(result).toEqual({
            ...initialState,
            success: false,
            loading: false,
            errorMessage: action.payload.message,
          });
      });

      it("should return DELETE_CUSTOMER_FAILURE state", () => {
        const action = {
          type: TYPE.DELETE_CUSTOMER_FAILURE,
          payload: { message: "errorMessage" },
        };
        const result = customerReducer(
            initialState,
          action
        );
        expect(result).toEqual({
            ...initialState,
            success: false,
            loading: false,
            errorMessage: action.payload.message,
          });
      });

      it("should return CLEAR_ERROR_MESSAGE state", () => {
        const action = {
          type: TYPE.CLEAR_ERROR_MESSAGE,
        };
        const result = customerReducer(
            initialState,
          action
        );
        expect(result).toEqual({
            ...initialState,
            errorMessage: null,
          });
      });
});