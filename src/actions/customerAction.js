import axios from "axios";
import * as TYPE from "./action-types";

//axios.defaults.baseURL = "http://localhost:8090/api"
export const fetchCustomerBegin = () => {
  return {
    type: TYPE.FETCH_CUSTOMERS_BEGIN,
  };
};

export const fetchCustomerError = (err) => {
  return {
    type: TYPE.FETCH_CUSTOMERS_FAILURE,
    payload: {
      message: "Failed to fetch cutomer.. please try again later!!!",
    },
  };
};

export const fetchCustomerSuccess = (cust) => {
  return {
    type: TYPE.FETCH_CUSTOMERS_SUCCESS,
    payload: cust,
  };
};

export const fetchCustomers = () => {
  return (dispatch, getState) => {
    dispatch(fetchCustomerBegin());
    axios.get("http://localhost:8080/api/customer/all").then(
      (res) => {
        dispatch(fetchCustomerSuccess(res.data.customers));
      },
      (err) => {
        dispatch(fetchCustomerError(err));
      }
    );
  };
};

export const updateCustomerBegin = () => {
  return {
    type: TYPE.SAVE_UPDATE_CUSTOMER_BEGIN,
  };
};

export const updateCustomerSuccess = (customer) => {
  return {
    type: TYPE.SAVE_UPDATE_CUSTOMER_SUCCESS,
    payload: customer,
  };
};

export const updateCustomerFailure = (errorMessage) => {
  return {
    type: TYPE.SAVE_UPDATE_CUSTOMER_FAILURE,
    payload: { message: errorMessage },
  };
};

export const SaveAndUpdateCustomer = (customer) => {
  return (dispatch, getState) => {
    dispatch(updateCustomerBegin());
    let url =
      "http://localhost:8080/api/customer/" + (customer.id ? "update" : "save");
    let method = customer.id ? "put" : "post";
    return axios({
      method: method,
      url: url,
      data: customer,
    }).then(
      (response) => {
        if (response.data && response.data.success) {
          dispatch(updateCustomerSuccess(response.data));
          return true;
        } else {
          dispatch(updateCustomerFailure(response.data.errorMessage));
          return false;
        }
      },
      (err) => {
        dispatch(updateCustomerFailure(err.message));
        return false;
      }
    );
  };
};

export const deleteCustomerBegin = () => {
  return {
    type: TYPE.DELETE_CUSTOMER_BEGIN,
  };
};

export const deleteCustomerSuccess = (customer) => {
  return {
    type: TYPE.DELETE_CUSTOMER_SUCCESS,
    payload: customer,
  };
};

export const deleteCustomerFailure = (errorMessage) => {
  return {
    type: TYPE.DELETE_CUSTOMER_FAILURE,
    payload: "Failed to cutomer disable/enable.. please try again later!!!",
  };
};

export const deleteCustomer = (customer) => {
  return (dispatch, getState) => {
    dispatch(deleteCustomerBegin());

    return axios
      .put("http://localhost:8080/api/customer/delete", customer)
      .then(
        (response) => {
          if (response.data && response.data.success) {
            dispatch(deleteCustomerSuccess(response.data));
          } else {
            dispatch(deleteCustomerFailure(response.data.errorMessage));
          }
        },
        (err) => {
          dispatch(deleteCustomerFailure(err.message));
        }
      );
  };
};

export const clearErrorMeaage = () => {
  return {
    type: TYPE.CLEAR_ERROR_MESSAGE,
  };
};
