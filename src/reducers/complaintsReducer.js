import * as TYPE from "./../actions/action-types";

const initialState = { entities: [], loading: true, error: null };

const complaintsReducer = (state = initialState, action) => {
  switch (action.type) {
    case TYPE.FETCH_COMPLAINTS_BEGIN:
      return { ...state, loading: true, error: null };
    case TYPE.FETCH_COMPLAINTS_SUCCESS:
      return { ...state, entities: action.payload, loading: false };
    case TYPE.FETCH_COMPLAINTS_FAILURE:
      return { ...state, entities: [], loading: false, error: action.payload };
    case TYPE.FETCH_STATUS_BEGIN:
    case TYPE.FETCH_STATUS_FAILURE:
      return { ...state, complaintStatus: ["Select"] };
    case TYPE.FETCH_STATUS_SUCCESS:
      return {
        ...state,
        complaintStatus: ["Select", ...action.payload],
      };
    case TYPE.ADD_COMPLAINT_BEGIN:
    case TYPE.EDIT_COMPLAINT_BEGIN:
      return {
        ...state,
        addOrEdit: false,
        loading: true,
        errorAddOrEdit: null,
      };
    case TYPE.ADD_COMPLAINT_FAILURE:
    case TYPE.EDIT_COMPLAINT_FAILURE:
      return {
        ...state,
        addOrEdit: false,
        loading: false,
        errorAddOrEdit: action.payload,
      };
    case TYPE.ADD_COMPLAINT_SUCCESS:
    case TYPE.EDIT_COMPLAINT_SUCCESS:
      return {
        ...state,
        addOrEdit: true,
        loading: false,
        errorAddOrEdit: null,
      };
    default:
      return state;
  }
};

export default complaintsReducer;
