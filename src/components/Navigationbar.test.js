import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";

import Navigationbar from "./Navigationbar";
import { MemoryRouter } from "react-router-dom";

let container = null;

beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("render Navigationbar component", () => {
  act(() => {
    render(
      <MemoryRouter>
        <Navigationbar />
      </MemoryRouter>,
      container
    );
  });
  expect(container.textContent).toBe("DashboardCustomerComplaints");
});
