import { connect, useDispatch } from "react-redux";
import Chart from "react-google-charts";
import { getChartData } from "../../actions/dashboardAction";
import React, { useEffect } from "react";
const Dashboard = (props) => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getChartData());
  }, [dispatch]);

  return (
    <div className="position-relative overflow-hidden p-3 p-md-5 m-md-3 text-center bg-light">
      <Chart
        chartType="BarChart"
        width="100%"
        height="400px"
        data={props.chartData}
        options={{
          title: "Customer complaint Vs Status",
          hAxis: { minValue: 10 },
          vAxis: { title: "Status" },
          bubble: { textStyle: { fontSize: 11 } },
        }}
      />
      <div className="product-device shadow-sm d-none d-md-block"></div>
      <div className="product-device product-device-2 shadow-sm d-none d-md-block"></div>
    </div>
  );
};

const mapStateToProps = (state) => {
  return {
    chartData: state.dashboard.chartData,
  };
};

export default connect(mapStateToProps)(Dashboard);
