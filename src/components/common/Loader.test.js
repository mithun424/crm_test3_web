import Loader from "../common/Loader";
import React from "react";
import { act } from "react-dom/test-utils";
import { render, unmountComponentAtNode } from "react-dom";

let container = null;

beforeEach(() => {
  // setup a DOM element as a render target
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  // cleanup on exiting
  unmountComponentAtNode(container);
  container.remove();
  container = null;
});

it("Loader component is loaded", () => {
    act(() => {
        render(<Loader/>, container);
      });
   expect(container.querySelector(".sr-only").textContent).toBe("Loading...");
});