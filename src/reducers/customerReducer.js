import * as TYPE from "./../actions/action-types";

const initialState = { entities: [], loading: false, error: null };

const customerReducer = (state = initialState, action) => {
  switch (action.type) {
    case TYPE.FETCH_CUSTOMERS_BEGIN:
      return { ...state, loading: true, error: null };
    case TYPE.FETCH_CUSTOMERS_SUCCESS:
      return { ...state, entities: action.payload, loading: false };
    case TYPE.FETCH_CUSTOMERS_FAILURE:
      return { ...state, entities: [], loading: false, error: action.payload };
    case TYPE.SAVE_UPDATE_CUSTOMER_BEGIN:
    case TYPE.DELETE_CUSTOMER_BEGIN:
      return { ...state, success: false, loading: true };
    case TYPE.SAVE_UPDATE_CUSTOMER_SUCCESS:
    case TYPE.DELETE_CUSTOMER_SUCCESS:
      return { ...state, success: true, loading: false };
    case TYPE.SAVE_UPDATE_CUSTOMER_FAILURE:
    case TYPE.DELETE_CUSTOMER_FAILURE:
      return {
        ...state,
        success: false,
        loading: false,
        errorMessage: action.payload.message,
      };
    case TYPE.CLEAR_ERROR_MESSAGE:
      return {
        ...state,
        errorMessage: null,
      };
    default:
      return state;
  }
};

export default customerReducer;
