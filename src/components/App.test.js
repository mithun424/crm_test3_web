import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";
import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";

import App from "./App";
import { Provider } from "react-redux";

describe("ShowCompliants testing", () => {
  let container = null;
  const middlewares = [thunk];
  const mockStore = configureMockStore(middlewares);

  beforeEach(() => {
    container = document.createElement("div");
    document.body.appendChild(container);
  });

  afterEach(() => {
    unmountComponentAtNode(container);
    container.remove();
    container = null;
  });

  it("render App component", () => {
    let store = mockStore({
      complaints: {
        entities: [],
        loading: true,
        error: null,
      },
      dashboard: { chartData: {} },
    });
    act(() => {
      render(
        <Provider store={store}>
          <App />
        </Provider>,
        container
      );
    });
    expect(container.textContent).toMatch(/Customer/i);
  });
});
