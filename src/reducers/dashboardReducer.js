import * as TYPE from "./../actions/action-types";

const initialState = { chartData: [], loading: false, error: null };

const dashboardReducer = (state = initialState, action) => {
  switch (action.type) {
      case TYPE.GET_CHARTDATA_BEGIN:
          return {
            ...state, loading: true
          }
      case TYPE.GET_CHARTDATA_SUCCESS:
        return {
          ...state, chartData: action.payload
        }
    default:
      return state;
  }
};

export default dashboardReducer;
