import React, { useCallback, useEffect } from "react";
import Customer from "./Customer";
import Table from "react-bootstrap/Table";
import { useSelector, useDispatch } from "react-redux";
import UpdateCustomer from "./UpdateCustomer";
import { fetchCustomers } from "../../actions/customerAction";
import { Alert } from "react-bootstrap";

const CustomerListing = () => {
	const dispatch = useDispatch();

	const initFetchCustomers = useCallback(() => {
		dispatch(fetchCustomers());
	}, [dispatch]);

	useEffect(() => {
		initFetchCustomers();
	}, [initFetchCustomers]);

	const customers = useSelector((state) => state.customer.entities);
	const error = useSelector((state) => state.customer.error);
	const success = useSelector((state) => state.customer.success);

	if (error) {
		return <div className="d-flex justify-content-center">{error.message}</div>;
	}

	return (
		<div>
			<UpdateCustomer customer={{}} />

			<br />
			<br />
			{success && <Alert variant={"success"}>Data Saved Successful</Alert>}
			<div>
				<Table striped bordered hover>
					<thead>
						<tr>
							<th>Name</th>
							<th>Address</th>
							<th>Email</th>
							<th>PhoneNumber</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						{customers.map((customer) => (
							<Customer key={customer.customerId} customer={customer} />
						))}
					</tbody>
				</Table>
			</div>
		</div>
	);
};

export default CustomerListing;
