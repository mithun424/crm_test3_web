import * as TYPE from "./../actions/action-types";
import complaintsReducer from "./complaintsReducer";

describe("complaintsReducer", () => {
  it("should return initial state", () => {
    const result = complaintsReducer(undefined, {});
    expect(result).toEqual({ entities: [], loading: true, error: null });
  });

  it("should return FETCH_COMPLAINTS_BEGIN state", () => {
    const action = {
      type: TYPE.FETCH_COMPLAINTS_BEGIN,
    };
    const result = complaintsReducer(
      { entities: [], loading: true, error: null },
      action
    );
    expect(result).toEqual({ entities: [], loading: true, error: null });
  });

  it("should return FETCH_STATUS_BEGIN state", () => {
    const action = {
      type: TYPE.FETCH_STATUS_BEGIN,
    };
    const result = complaintsReducer(
      { entities: [], loading: true, error: null },
      action
    );
    expect(result).toEqual({
      entities: [],
      loading: true,
      error: null,
      complaintStatus: ["Select"],
    });
  });

  it("should return FETCH_STATUS_FAILURE state", () => {
    const action = {
      type: TYPE.FETCH_STATUS_FAILURE,
    };
    const result = complaintsReducer(
      { entities: [], loading: true, error: null },
      action
    );
    expect(result).toEqual({
      entities: [],
      loading: true,
      error: null,
      complaintStatus: ["Select"],
    });
  });

  it("should return ADD_COMPLAINT_BEGIN state", () => {
    const action = {
      type: TYPE.ADD_COMPLAINT_BEGIN,
    };
    const result = complaintsReducer(
      { entities: [], loading: true, error: null },
      action
    );
    expect(result).toEqual({
      entities: [],
      error: null,
      addOrEdit: false,
      loading: true,
      errorAddOrEdit: null,
    });
  });

  it("should return EDIT_COMPLAINT_BEGIN state", () => {
    const action = {
      type: TYPE.EDIT_COMPLAINT_BEGIN,
    };
    const result = complaintsReducer(
      { entities: [], loading: true, error: null },
      action
    );
    expect(result).toEqual({
      entities: [],
      error: null,
      addOrEdit: false,
      loading: true,
      errorAddOrEdit: null,
    });
  });

  it("should return FETCH_COMPLAINTS_SUCCESS state", () => {
    const action = {
      type: TYPE.FETCH_COMPLAINTS_SUCCESS,
      payload: [
        {
          commentId: "42c8150e-6663-4649-a486-175da825a781",
          customerId: "4b1a32da-01d6-4f86-ae8a-8dd68b1d63c3",
          customerName: "Sudham P",
          title: "Title 2",
          description: "Desc 23",
          status: "OPEN",
          assignee: "0298b44d-b45a-47af-a09d-ecc9bebd88fb",
          assigneeName: "Mithun R",
        },
      ],
    };
    const result = complaintsReducer(
      { entities: [], loading: true, error: null },
      action
    );
    expect(result).toEqual({
      entities: [
        {
          commentId: "42c8150e-6663-4649-a486-175da825a781",
          customerId: "4b1a32da-01d6-4f86-ae8a-8dd68b1d63c3",
          customerName: "Sudham P",
          title: "Title 2",
          description: "Desc 23",
          status: "OPEN",
          assignee: "0298b44d-b45a-47af-a09d-ecc9bebd88fb",
          assigneeName: "Mithun R",
        },
      ],
      loading: false,
      error: null,
    });
  });

  it("should return FETCH_STATUS_SUCCESS state", () => {
    const action = {
      type: TYPE.FETCH_STATUS_SUCCESS,
      payload: ["OPEN", "INPROGRESS", "REVIEW", "DONE"],
    };
    const result = complaintsReducer(
      { entities: [], loading: true, error: null },
      action
    );
    expect(result).toEqual({
      entities: [],
      loading: true,
      error: null,
      complaintStatus: ["Select", "OPEN", "INPROGRESS", "REVIEW", "DONE"],
    });
  });

  it("should return FETCH_COMPLAINTS_FAILURE state", () => {
    const action = {
      type: TYPE.FETCH_COMPLAINTS_FAILURE,
      payload: {
        message: "Failed to fetch complaints.. please try again later!!!",
      },
    };
    const result = complaintsReducer(
      { entities: [], loading: true, error: null },
      action
    );
    expect(result).toEqual({
      entities: [],
      loading: false,
      error: {
        message: "Failed to fetch complaints.. please try again later!!!",
      },
    });
  });

  it("should return ADD_COMPLAINT_FAILURE state", () => {
    const action = {
      type: TYPE.ADD_COMPLAINT_FAILURE,
      payload: {
        message: "Failed to add complaint.. please try again later!!!",
      },
    };
    const result = complaintsReducer(
      { entities: [], loading: true, error: null },
      action
    );
    expect(result).toEqual({
      entities: [],
      error: null,
      addOrEdit: false,
      loading: false,
      errorAddOrEdit: {
        message: "Failed to add complaint.. please try again later!!!",
      },
    });
  });

  it("should return ADD_COMPLAINT_SUCCESS state", () => {
    const action = {
      type: TYPE.ADD_COMPLAINT_SUCCESS,
    };
    const result = complaintsReducer(
      { entities: [], loading: true, error: null },
      action
    );
    expect(result).toEqual({
      entities: [],
      error: null,
      addOrEdit: true,
      loading: false,
      errorAddOrEdit: null,
    });
  });

  it("should return EDIT_COMPLAINT_FAILURE state", () => {
    const action = {
      type: TYPE.EDIT_COMPLAINT_FAILURE,
      payload: {
        message: "Failed to edit complaint.. please try again later!!!",
      },
    };
    const result = complaintsReducer(
      { entities: [], loading: true, error: null },
      action
    );
    expect(result).toEqual({
      entities: [],
      error: null,
      addOrEdit: false,
      loading: false,
      errorAddOrEdit: {
        message: "Failed to edit complaint.. please try again later!!!",
      },
    });
  });

  it("should return EDIT_COMPLAINT_SUCCESS state", () => {
    const action = {
      type: TYPE.EDIT_COMPLAINT_SUCCESS,
    };
    const result = complaintsReducer(
      { entities: [], loading: true, error: null },
      action
    );
    expect(result).toEqual({
      entities: [],
      error: null,
      addOrEdit: true,
      loading: false,
      errorAddOrEdit: null,
    });
  });
});
