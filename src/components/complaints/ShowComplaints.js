import React, { useEffect, useCallback } from "react";
import { Table, Alert } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import { fetchComplaints } from "../../actions/complaintAction";
import EditComplaint from "./EditComplaint";
import AddComplaint from "./AddComplaint";

const ShowCompliants = () => {
  const dispatch = useDispatch();
  const complaints = useSelector((state) => state.complaints.entities);
  const error = useSelector((state) => state.complaints.error);
  const loading = useSelector((state) => state.complaints.loading);
  const addOrEdit = useSelector((state) => state.complaints.addOrEdit);
  const errorAddOrEdit = useSelector(
    (state) => state.complaints.errorAddOrEdit
  );
  const initFetchComplaints = useCallback(() => {
    dispatch(fetchComplaints());
  }, [dispatch]);

  useEffect(() => {
    initFetchComplaints();
  }, [initFetchComplaints]);

  if (error) {
    return <div className="d-flex justify-content-center">{error.message}</div>;
  }
  if (loading) {
    return (
      <div className="d-flex justify-content-center">
        <div
          className="spinner-border m-5"
          style={{ width: "4rem", height: "4rem" }}
          role="status"
        >
          <span className="sr-only">Loading...</span>
        </div>
      </div>
    );
  }
  return (
    <>
      <AddComplaint />
      <br /> <br />
      {errorAddOrEdit && (
        <Alert variant={"danger"}>{errorAddOrEdit.message}</Alert>
      )}
      {addOrEdit && <Alert variant={"success"}>Data Saved Successful</Alert>}
      <Table responsive="sm" striped bordered hover>
        <thead>
          <tr>
            <th>Customer Name</th>
            <th>Complaint Title</th>
            <th>Complaint Desc</th>
            <th>Status</th>
            <th>Assign To</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {complaints.map((complaint) => (
            <tr key={complaint.commentId}>
              <td>{complaint.customerName}</td>
              <td>{complaint.title}</td>
              <td>{complaint.description}</td>
              <td>{complaint.status}</td>
              <td>{complaint.assigneeName}</td>
              <td>
                <EditComplaint complaint={complaint} />
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </>
  );
};

export default ShowCompliants;
