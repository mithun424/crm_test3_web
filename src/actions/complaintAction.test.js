import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import * as TYPE from "./action-types";
import * as complaintAction from "./complaintAction";
import axios from "axios";

jest.mock("axios");

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe("complaintAction", () => {
  it("should create an action to FETCH_COMPLAINTS_BEGIN", () => {
    const expectedAction = {
      type: TYPE.FETCH_COMPLAINTS_BEGIN,
    };
    expect(complaintAction.fetchComplaintsBegin()).toEqual(expectedAction);
  });

  it("should create an action to FETCH_COMPLAINTS_SUCCESS", () => {
    const complaints = [
      {
        commentId: "42c8150e-6663-4649-a486-175da825a781",
        customerId: "4b1a32da-01d6-4f86-ae8a-8dd68b1d63c3",
        customerName: "Sudham P",
        title: "Title 2",
        description: "Desc 23",
        status: "OPEN",
        assignee: "0298b44d-b45a-47af-a09d-ecc9bebd88fb",
        assigneeName: "Mithun R",
      },
    ];
    const expectedAction = {
      type: TYPE.FETCH_COMPLAINTS_SUCCESS,
      payload: complaints,
    };
    expect(complaintAction.fetchComplaintsSuccess(complaints)).toEqual(
      expectedAction
    );
  });

  it("should create an action to FETCH_COMPLAINTS_FAILURE", () => {
    const error = {
      message: "Failed to fetch complaints.. please try again later!!!",
    };
    const expectedAction = {
      type: TYPE.FETCH_COMPLAINTS_FAILURE,
      payload: error,
    };
    expect(complaintAction.fetchComplaintsFailure(error)).toEqual(
      expectedAction
    );
  });

  it("fetchComplaints success", () => {
    axios.get.mockImplementationOnce(() =>
      Promise.resolve({ data: { comments: [] } })
    );

    const store = mockStore({ entities: [], loading: true, error: null });
    store.dispatch(complaintAction.fetchComplaints());
  });

  it("fetchComplaints failure", () => {
    axios.get.mockImplementationOnce(() => Promise.reject());

    const store = mockStore({ entities: [], loading: true, error: null });
    store.dispatch(complaintAction.fetchComplaints());
  });

  it("should create an action to FETCH_STATUS_BEGIN", () => {
    const expectedAction = {
      type: TYPE.FETCH_STATUS_BEGIN,
    };
    expect(complaintAction.fetchStatusBegin()).toEqual(expectedAction);
  });

  it("should create an action to FETCH_STATUS_SUCCESS", () => {
    const status = ["OPEN", "INPROGRESS", "REVIEW", "DONE"];
    const expectedAction = {
      type: TYPE.FETCH_STATUS_SUCCESS,
      payload: status,
    };
    expect(complaintAction.fetchStatusSuccess(status)).toEqual(expectedAction);
  });

  it("should create an action to FETCH_STATUS_FAILURE", () => {
    const expectedAction = {
      type: TYPE.FETCH_STATUS_FAILURE,
    };
    expect(complaintAction.fetchStatusFailure()).toEqual(expectedAction);
  });

  it("fetchStatus success", () => {
    axios.get.mockImplementationOnce(() =>
      Promise.resolve({ data: { statusList: [] } })
    );

    const store = mockStore({ entities: [], loading: true, error: null });
    store.dispatch(complaintAction.fetchStatus());
  });

  it("fetchStatus failure", () => {
    axios.get.mockImplementationOnce(() => Promise.reject());

    const store = mockStore({ entities: [], loading: true, error: null });
    store.dispatch(complaintAction.fetchStatus());
  });

  it("should create an action to ADD_COMPLAINT_BEGIN", () => {
    const expectedAction = {
      type: TYPE.ADD_COMPLAINT_BEGIN,
    };
    expect(complaintAction.addComplaintBegin()).toEqual(expectedAction);
  });

  it("should create an action to ADD_COMPLAINT_SUCCESS", () => {
    const expectedAction = {
      type: TYPE.ADD_COMPLAINT_SUCCESS,
    };
    expect(complaintAction.addComplaintSuccess()).toEqual(expectedAction);
  });

  it("should create an action to ADD_COMPLAINT_FAILURE", () => {
    const error = {
      message: "Failed to add complaint.. please try again later!!!",
    };
    const expectedAction = {
      type: TYPE.ADD_COMPLAINT_FAILURE,
      payload: error,
    };
    expect(complaintAction.addComplaintFailure(error)).toEqual(expectedAction);
  });

  it("addComplaint success", () => {
    axios.post.mockImplementationOnce(() => Promise.resolve(true));
    const expectActions = [{ type: TYPE.ADD_COMPLAINT_SUCCESS }];
    const store = mockStore({ entities: [], loading: true, error: null });
    return store.dispatch(complaintAction.addComplaint()).then(() => {
      expect(store.getActions()).toEqual(expectActions);
    });
  });

  it("addComplaint failure", () => {
    const error = {
      message: "Failed to add complaint.. please try again later!!!",
    };
    axios.post.mockImplementationOnce(() => Promise.reject());
    const expectActions = [
      { type: TYPE.ADD_COMPLAINT_FAILURE, payload: error },
    ];
    const store = mockStore({ entities: [], loading: true, error: null });
    return store.dispatch(complaintAction.addComplaint()).then(() => {
      expect(store.getActions()).toEqual(expectActions);
    });
  });

  it("should create an action to EDIT_COMPLAINT_BEGIN", () => {
    const expectedAction = {
      type: TYPE.EDIT_COMPLAINT_BEGIN,
    };
    expect(complaintAction.editComplaintBegin()).toEqual(expectedAction);
  });

  it("should create an action to EDIT_COMPLAINT_SUCCESS", () => {
    const expectedAction = {
      type: TYPE.EDIT_COMPLAINT_SUCCESS,
    };
    expect(complaintAction.editComplaintSuccess()).toEqual(expectedAction);
  });

  it("should create an action to EDIT_COMPLAINT_FAILURE", () => {
    const error = {
      message: "Failed to edit complaint.. please try again later!!!",
    };
    const expectedAction = {
      type: TYPE.EDIT_COMPLAINT_FAILURE,
      payload: error,
    };
    expect(complaintAction.editComplaintFailure(error)).toEqual(expectedAction);
  });

  it("editComplaint success", () => {
    axios.put.mockImplementationOnce(() => Promise.resolve(true));
    const expectActions = [{ type: TYPE.EDIT_COMPLAINT_SUCCESS }];
    const store = mockStore({ entities: [], loading: true, error: null });
    return store.dispatch(complaintAction.editComplaint()).then(() => {
      expect(store.getActions()).toEqual(expectActions);
    });
  });

  it("editComplaint failure", () => {
    const error = {
      message: "Failed to edit complaint.. please try again later!!!",
    };
    axios.put.mockImplementationOnce(() => Promise.reject());
    const expectActions = [
      { type: TYPE.EDIT_COMPLAINT_FAILURE, payload: error },
    ];
    const store = mockStore({ entities: [], loading: true, error: null });
    return store.dispatch(complaintAction.editComplaint()).then(() => {
      expect(store.getActions()).toEqual(expectActions);
    });
  });
});
