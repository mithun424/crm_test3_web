import React from "react";
import Button from "react-bootstrap/Button";
import UpdateCustomer from "./UpdateCustomer";
import { useDispatch } from "react-redux";
import { deleteCustomer, fetchCustomers } from "../../actions/customerAction";

const Customer = (props) => {
  const dispatch = useDispatch();
  const handleActivation = async () => {
    await dispatch(deleteCustomer(props.customer));
    dispatch(fetchCustomers());
  };
  return (
    <tr key={props.customer.id}>
      <td>
        {props.customer.firstName} {props.customer.lastName}
      </td>
      <td>{props.customer.address}</td>
      <td>{props.customer.email}</td>
      <td>{props.customer.mobile}</td>
      <td>
        <UpdateCustomer customer={props.customer} />{" "}
        <Button variant="outline-warning" onClick={handleActivation}>
          {props.customer.active ? "Disable" : "Enable"}
        </Button>
      </td>
    </tr>
  );
};

export default Customer;
