import React from "react";
import { NavLink } from "react-router-dom";
import { Navbar, Nav } from "react-bootstrap";

const Navigationbar = () => (
  <Navbar bg="primary" variant="dark" expand="lg">
    <Navbar.Toggle aria-controls="basic-navbar-nav" />
    <Navbar.Collapse id="basic-navbar-nav">
      <Nav className="mr-auto">
        <NavLink exact className="nav-link" to="/">
          Dashboard
        </NavLink>
        <NavLink exact className="nav-link" to="/customer">
          Customer
        </NavLink>
        <NavLink exact className="nav-link" to="/complaints">
          Complaints
        </NavLink>
      </Nav>
    </Navbar.Collapse>
  </Navbar>
);

export default Navigationbar;
