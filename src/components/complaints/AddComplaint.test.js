import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";
import { Provider } from "react-redux";
import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";

import AddComplaint from "./AddComplaint";

describe("AddComplaint testing", () => {
  let container = null;
  const middlewares = [thunk];
  const mockStore = configureMockStore(middlewares);

  beforeEach(() => {
    container = document.createElement("div");
    document.body.appendChild(container);
  });

  afterEach(() => {
    unmountComponentAtNode(container);
    container.remove();
    container = null;
  });

  it("render AddComplaint component", () => {
    let store = mockStore({
      complaints: { entities: [], loading: true, error: null },
      customer: {
        entities: [
          {
            id: "5fc5ffbd73805c310202e6de",
            customerId: "0298b44d-b45a-47af-a09d-ecc9bebd88fb",
            firstName: "Mithun",
            lastName: "R",
            email: "m@m.com",
            mobile: "98145353213",
            address: "test",
            role: "ADMIN",
            active: true,
          },
          {
            id: "5fc6175ab2aab95e29ac73b7",
            customerId: "4b1a32da-01d6-4f86-ae8a-8dd68b1d63c3",
            firstName: "Sudham",
            lastName: "P",
            email: "s@p.com",
            mobile: "1234567890",
            address: "test12",
            role: "CUSTOMER",
            active: true,
          },
          {
            id: "5fc72be48176936069784dfc",
            customerId: "7dacf35b-2e12-4527-9e91-b73ebef2f69e",
            firstName: "Yesh",
            lastName: "S",
            email: "y@s.com",
            mobile: "1234567890",
            address: "test123",
            role: "CUSTOMER",
            active: true,
          },
        ],
      },
    });
    act(() => {
      render(
        <Provider store={store}>
          <AddComplaint />
        </Provider>,
        container
      );
    });
    expect(container.textContent).toMatch(/Add Complaints/i);
  });
});
