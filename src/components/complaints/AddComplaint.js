import React, { useState, useCallback, useEffect, useRef } from "react";
import { Button, Modal, Form } from "react-bootstrap";
import { useDispatch, useSelector } from "react-redux";
import {
  fetchStatus,
  addComplaint,
  fetchComplaints,
} from "../../actions/complaintAction";
import { fetchCustomers } from "../../actions/customerAction";

const AddComplaint = () => {
  const [show, setShow] = useState(false);
  const [validated, setValidated] = useState(false);
  const formRef = useRef(null);
  const dispatch = useDispatch();

  const initFetchStatus = useCallback(() => {
    dispatch(fetchStatus());
  }, [dispatch]);

  const initFetchCustomers = useCallback(() => {
    dispatch(fetchCustomers());
  }, [dispatch]);

  useEffect(() => {
    initFetchStatus();
    initFetchCustomers();
  }, [initFetchStatus, initFetchCustomers]);

  const complaintStatus = useSelector(
    (state) => state.complaints.complaintStatus
  );

  const assigns = useSelector((state) =>
    state.customer.entities.filter((e) => e.role === "ADMIN")
  );
  const customers = useSelector((state) =>
    state.customer.entities.filter((e) => e.role === "CUSTOMER")
  );

  const handleClose = () => {
    setShow(false);
    handleFormReset();
  };

  const handleFormReset = () => {
    formRef.current.reset();
    setValidated(false);
    setTitle("");
    setAssign("");
    setCustomerId("");
    setDesc("");
    setStatus("");
  };

  const handleShow = () => setShow(true);
  const handleSave = async (event) => {
    event.preventDefault();
    event.stopPropagation();
    const form = event.currentTarget;
    if (!form.checkValidity()) {
      setValidated(true);
    } else {
      const complaint = {
        customerId: customerId,
        title: title,
        description: desc,
        assignee: assign,
        status: status,
      };
      const result = await dispatch(addComplaint(complaint));
      if (result) {
        dispatch(fetchComplaints());
      }
      setShow(false);
      handleFormReset();
    }
  };
  const [title, setTitle] = useState("");
  const [customerId, setCustomerId] = useState("");
  const [desc, setDesc] = useState("");
  const [status, setStatus] = useState("");
  const [assign, setAssign] = useState("");

  return (
    <>
      <Button variant="primary" onClick={handleShow}>
        Add Complaints
      </Button>

      <Modal show={show} backdrop="static" onHide={handleClose}>
        <Form
          ref={formRef}
          noValidate
          validated={validated}
          onSubmit={handleSave}
        >
          <Modal.Header closeButton>
            <Modal.Title>Add Complaint</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form.Group controlId="customerNamesSelect">
              <Form.Label>Customer Names</Form.Label>
              <Form.Control
                required
                as="select"
                value={customerId}
                onChange={(e) => setCustomerId(e.target.value)}
              >
                <option value="">Select</option>
                {customers &&
                  customers.map((customer) => (
                    <option
                      key={customer.customerId}
                      value={customer.customerId}
                    >
                      {customer.firstName + " " + customer.lastName}
                    </option>
                  ))}
              </Form.Control>
              <Form.Control.Feedback type="invalid">
                Please choose a customer names.
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group controlId="complaintTile">
              <Form.Label>Complaint Title</Form.Label>
              <Form.Control
                required
                type="text"
                value={title}
                placeholder="Complaint Title"
                onChange={(e) => setTitle(e.target.value)}
              />
              <Form.Control.Feedback type="invalid">
                Please choose a complaint title.
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group controlId="complaintDec">
              <Form.Label>Complaint Description</Form.Label>
              <Form.Control
                required
                as="textarea"
                placeholder="Complaint Description"
                value={desc}
                onChange={(e) => setDesc(e.target.value)}
              />
              <Form.Control.Feedback type="invalid">
                Please choose a complaint description.
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group controlId="AssignToSelect">
              <Form.Label>Assign To</Form.Label>
              <Form.Control
                required
                as="select"
                value={assign}
                onChange={(e) => setAssign(e.target.value)}
              >
                <option value="">Select</option>
                {assigns.map((assign) => (
                  <option key={assign.customerId} value={assign.customerId}>
                    {assign.firstName + " " + assign.lastName}
                  </option>
                ))}
              </Form.Control>
              <Form.Control.Feedback type="invalid">
                Please choose a assign to.
              </Form.Control.Feedback>
            </Form.Group>
            <Form.Group controlId="ComplaintStatusSelect">
              <Form.Label>Complaint Status</Form.Label>
              <Form.Control
                required
                as="select"
                value={status}
                onChange={(e) => setStatus(e.target.value)}
              >
                {complaintStatus &&
                  complaintStatus.map((status) => (
                    <option
                      key={status}
                      value={status.toLowerCase() === "select" ? "" : status}
                    >
                      {status}
                    </option>
                  ))}
              </Form.Control>
              <Form.Control.Feedback type="invalid">
                Please choose a complaint status.
              </Form.Control.Feedback>
            </Form.Group>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
            <Button type="submit" variant="primary">
              Save
            </Button>
          </Modal.Footer>
        </Form>
      </Modal>
    </>
  );
};

export default AddComplaint;
