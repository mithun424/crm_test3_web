import axios from "axios";
import * as TYPE from "./action-types";

export const fetchComplaintsBegin = () => {
  return {
    type: TYPE.FETCH_COMPLAINTS_BEGIN,
  };
};

export const fetchComplaintsSuccess = (complaints) => {
  return {
    type: TYPE.FETCH_COMPLAINTS_SUCCESS,
    payload: complaints,
  };
};
export const fetchComplaintsFailure = (err) => {
  return {
    type: TYPE.FETCH_COMPLAINTS_FAILURE,
    payload: {
      message: "Failed to fetch complaints.. please try again later!!!",
    },
  };
};

export const fetchComplaints = () => {
  return (dispatch, getState) => {
    dispatch(fetchComplaintsBegin);

    axios.get("http://localhost:8080/api/comment/all").then(
      (resp) => {
        dispatch(fetchComplaintsSuccess(resp.data.comments));
      },
      (err) => {
        dispatch(fetchComplaintsFailure(err));
      }
    );
  };
};

export const fetchStatusBegin = () => {
  return {
    type: TYPE.FETCH_STATUS_BEGIN,
  };
};

export const fetchStatusSuccess = (status) => {
  return {
    type: TYPE.FETCH_STATUS_SUCCESS,
    payload: status,
  };
};
export const fetchStatusFailure = () => {
  return {
    type: TYPE.FETCH_STATUS_FAILURE,
  };
};

export const fetchStatus = () => {
  return (dispatch, getState) => {
    dispatch(fetchStatusBegin);

    axios.get("http://localhost:8080/api/common/status").then(
      (resp) => {
        dispatch(fetchStatusSuccess(resp.data.statusList));
      },
      (err) => {
        dispatch(fetchStatusFailure());
      }
    );
  };
};

export const addComplaintBegin = () => {
  return {
    type: TYPE.ADD_COMPLAINT_BEGIN,
  };
};

export const addComplaintSuccess = () => {
  return {
    type: TYPE.ADD_COMPLAINT_SUCCESS,
  };
};
export const addComplaintFailure = (err) => {
  return {
    type: TYPE.ADD_COMPLAINT_FAILURE,
    payload: {
      message: "Failed to add complaint.. please try again later!!!",
    },
  };
};

export const addComplaint = (complaint) => {
  return (dispatch, getState) => {
    dispatch(addComplaintBegin);

    return axios.post("http://localhost:8080/api/comment/save", complaint).then(
      (resp) => {
        dispatch(addComplaintSuccess());
        return true;
      },
      (err) => {
        dispatch(addComplaintFailure(err));
        return false;
      }
    );
  };
};

export const editComplaintBegin = () => {
  return {
    type: TYPE.EDIT_COMPLAINT_BEGIN,
  };
};

export const editComplaintSuccess = () => {
  return {
    type: TYPE.EDIT_COMPLAINT_SUCCESS,
  };
};
export const editComplaintFailure = (err) => {
  return {
    type: TYPE.EDIT_COMPLAINT_FAILURE,
    payload: {
      message: "Failed to edit complaint.. please try again later!!!",
    },
  };
};

export const editComplaint = (complaint) => {
  return (dispatch, getState) => {
    dispatch(editComplaintBegin);

    return axios
      .put("http://localhost:8080/api/comment/update", complaint)
      .then(
        (resp) => {
          dispatch(editComplaintSuccess());
          return true;
        },
        (err) => {
          dispatch(editComplaintFailure(err));
          return false;
        }
      );
  };
};
