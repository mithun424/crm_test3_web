import { combineReducers } from "redux";
import customerReducer from "./customerReducer";
import complaintsReducer from "./complaintsReducer";
import dashboardReducer from "./dashboardReducer";

const rootReducer = combineReducers({
  customer: customerReducer,
  complaints: complaintsReducer,
  dashboard: dashboardReducer,
});

export default rootReducer;
