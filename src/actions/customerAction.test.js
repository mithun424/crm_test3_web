import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";
import * as TYPE from "./action-types";
import * as customerAction from "./customerAction";
import axios from "axios";

jest.mock("axios");

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe("customerAction", () => {
  it("should create an action to FETCH_CUSTOMERS_BEGIN", () => {
    const expectedAction = {
      type: TYPE.FETCH_CUSTOMERS_BEGIN,
    };
    expect(customerAction.fetchCustomerBegin()).toEqual(expectedAction);
  });

  it("should create an action to FETCH_CUSTOMERS_SUCCESS", () => {
    const customers = [
      {
        id: "42c8150e-6663-4649-a486-175da825a781",
        customerId: "4b1a32da-01d6-4f86-ae8a-8dd68b1d63c3",
        firstName: "Sudham P",
        lastName: "Title 2",
        email: "Desc 23",
        mobile: "OPEN",
        role: "0298b44d-b45a-47af-a09d-ecc9bebd88fb",
      },
    ];
    const expectedAction = {
      type: TYPE.FETCH_CUSTOMERS_SUCCESS,
      payload: customers,
    };
    expect(customerAction.fetchCustomerSuccess(customers)).toEqual(
      expectedAction
    );
  });

  it("should create an action to FETCH_CUSTOMERS_FAILURE", () => {
    const error = {
      message: "Failed to fetch cutomer.. please try again later!!!",
    };
    const expectedAction = {
      type: TYPE.FETCH_CUSTOMERS_FAILURE,
      payload: error,
    };
    expect(customerAction.fetchCustomerError(error)).toEqual(expectedAction);
  });

  it("fetchCustomers success", () => {
    axios.get.mockImplementationOnce(() =>
      Promise.resolve({ data: { customers: [] } })
    );

    const store = mockStore({ entities: [], loading: true, error: null });
    store.dispatch(customerAction.fetchCustomers());
  });

  it("fetchCustomers failure", () => {
    axios.get.mockImplementationOnce(() => Promise.reject());

    const store = mockStore({ entities: [], loading: true, error: null });
    store.dispatch(customerAction.fetchCustomers());
  });

  it("should create an action to SAVE_UPDATE_CUSTOMER_BEGIN", () => {
    const expectedAction = {
      type: TYPE.SAVE_UPDATE_CUSTOMER_BEGIN,
    };
    expect(customerAction.updateCustomerBegin()).toEqual(expectedAction);
  });

  it("should create an action to SAVE_UPDATE_CUSTOMER_SUCCESS", () => {
    let customer = {
      id: "42c8150e-6663-4649-a486-175da825a781",
      customerId: "4b1a32da-01d6-4f86-ae8a-8dd68b1d63c3",
      firstName: "Sudham P",
      lastName: "Title 2",
      email: "Desc 23",
      mobile: "OPEN",
      role: "0298b44d-b45a-47af-a09d-ecc9bebd88fb",
    };
    const expectedAction = {
      type: TYPE.SAVE_UPDATE_CUSTOMER_SUCCESS,
      payload: customer,
    };
    expect(customerAction.updateCustomerSuccess(customer)).toEqual(
      expectedAction
    );
  });

  it("should create an action to SAVE_UPDATE_CUSTOMER_FAILURE", () => {
    const errorMessage = "Failed to edit customer.. please try again later!!!";
    const expectedAction = {
      type: TYPE.SAVE_UPDATE_CUSTOMER_FAILURE,
      payload: { message: errorMessage },
    };
    expect(customerAction.updateCustomerFailure(errorMessage)).toEqual(
      expectedAction
    );
  });

  it("SaveAndUpdateCustomer success", () => {
    axios.mockImplementationOnce(() =>
      Promise.resolve({ data: { success: true } })
    );
    const expectActions = [
      { type: TYPE.SAVE_UPDATE_CUSTOMER_BEGIN },
      {
        type: TYPE.SAVE_UPDATE_CUSTOMER_SUCCESS,
        payload: { success: true },
      },
    ];
    const store = mockStore({ entities: [], loading: true, error: null });
    let customer = {};
    return store
      .dispatch(customerAction.SaveAndUpdateCustomer(customer))
      .then(() => {
        expect(store.getActions()).toEqual(expectActions);
      });
  });

  it("SaveAndUpdateCustomer update success", () => {
    axios.mockImplementationOnce(() =>
      Promise.resolve({ data: { success: true } })
    );
    const expectActions = [
      { type: TYPE.SAVE_UPDATE_CUSTOMER_BEGIN },
      {
        type: TYPE.SAVE_UPDATE_CUSTOMER_SUCCESS,
        payload: { success: true },
      },
    ];
    const store = mockStore({ entities: [], loading: true, error: null });
    let customer = {
      id: "42c8150e-6663-4649-a486-175da825a781",
      customerId: "4b1a32da-01d6-4f86-ae8a-8dd68b1d63c3",
      firstName: "Sudham P",
      lastName: "Title 2",
      email: "Desc 23",
      mobile: "OPEN",
      role: "0298b44d-b45a-47af-a09d-ecc9bebd88fb",
    };
    return store
      .dispatch(customerAction.SaveAndUpdateCustomer(customer))
      .then(() => {
        expect(store.getActions()).toEqual(expectActions);
      });
  });

  it("SaveAndUpdateCustomer failure", () => {
    axios.mockImplementationOnce(() =>
      Promise.reject({
        err: { message: undefined },
      })
    );
    const expectActions = [
      { type: TYPE.SAVE_UPDATE_CUSTOMER_BEGIN },
      {
        type: TYPE.SAVE_UPDATE_CUSTOMER_FAILURE,
        payload: { message: undefined },
      },
    ];
    const store = mockStore({ entities: [], loading: true, error: null });
    let customer = {};
    return store
      .dispatch(customerAction.SaveAndUpdateCustomer(customer))
      .then(() => {
        expect(store.getActions()).toEqual(expectActions);
      });
  });

  it("should create an action to DELETE_CUSTOMER_BEGIN", () => {
    const expectedAction = {
      type: TYPE.DELETE_CUSTOMER_BEGIN,
    };
    expect(customerAction.deleteCustomerBegin()).toEqual(expectedAction);
  });

  it("should create an action to DELETE_CUSTOMER_SUCCESS", () => {
    let customer = {
      id: "42c8150e-6663-4649-a486-175da825a781",
      customerId: "4b1a32da-01d6-4f86-ae8a-8dd68b1d63c3",
      firstName: "Sudham P",
      lastName: "Title 2",
      email: "Desc 23",
      mobile: "OPEN",
      role: "0298b44d-b45a-47af-a09d-ecc9bebd88fb",
    };
    const expectedAction = {
      type: TYPE.DELETE_CUSTOMER_SUCCESS,
      payload: customer,
    };
    expect(customerAction.deleteCustomerSuccess(customer)).toEqual(
      expectedAction
    );
  });

  it("should create an action to DELETE_CUSTOMER_FAILURE", () => {
    const errorMessage =
      "Failed to cutomer disable/enable.. please try again later!!!";
    const expectedAction = {
      type: TYPE.DELETE_CUSTOMER_FAILURE,
      payload: errorMessage,
    };
    expect(customerAction.deleteCustomerFailure(errorMessage)).toEqual(
      expectedAction
    );
  });

  it("deleteCustomer success", () => {
    axios.put.mockImplementationOnce(() =>
      Promise.resolve({ data: { success: true } })
    );
    const expectActions = [
      { type: TYPE.DELETE_CUSTOMER_BEGIN },
      {
        type: TYPE.DELETE_CUSTOMER_SUCCESS,
        payload: { success: true },
      },
    ];
    const store = mockStore({ entities: [], loading: true, error: null });
    let customer = {};
    return store.dispatch(customerAction.deleteCustomer(customer)).then(() => {
      expect(store.getActions()).toEqual(expectActions);
    });
  });

  it("SaveAndUpdateCustomer failure", () => {
    axios.put.mockImplementationOnce(() =>
      Promise.reject({
        err: { message: undefined },
      })
    );
    const expectActions = [
      { type: TYPE.DELETE_CUSTOMER_BEGIN },
      {
        type: TYPE.DELETE_CUSTOMER_FAILURE,
        payload: "Failed to cutomer disable/enable.. please try again later!!!",
      },
    ];
    const store = mockStore({ entities: [], loading: true, error: null });
    let customer = {};
    return store.dispatch(customerAction.deleteCustomer(customer)).then(() => {
      expect(store.getActions()).toEqual(expectActions);
    });
  });

  it("should create an action to CLEAR_ERROR_MESSAGE", () => {
    const expectedAction = {
      type: TYPE.CLEAR_ERROR_MESSAGE,
    };
    expect(customerAction.clearErrorMeaage()).toEqual(expectedAction);
  });
});
