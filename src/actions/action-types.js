export const FETCH_COMPLAINTS_BEGIN = "FETCH_COMPLAINTS_BEGIN";
export const FETCH_COMPLAINTS_SUCCESS = "FETCH_COMPLAINTS_SUCCESS";
export const FETCH_COMPLAINTS_FAILURE = "FETCH_COMPLAINTS_FAILURE";

export const ADD_COMPLAINT_BEGIN = "ADD_COMPLAINT_BEGIN";
export const ADD_COMPLAINT_SUCCESS = "ADD_COMPLAINT_SUCCESS";
export const ADD_COMPLAINT_FAILURE = "ADD_COMPLAINT_FAILURE";

export const EDIT_COMPLAINT_BEGIN = "EDIT_COMPLAINT_BEGIN";
export const EDIT_COMPLAINT_SUCCESS = "EDIT_COMPLAINT_SUCCESS";
export const EDIT_COMPLAINT_FAILURE = "EDIT_COMPLAINT_FAILURE";

export const FETCH_STATUS_BEGIN = "FETCH_STATUS_BEGIN";
export const FETCH_STATUS_SUCCESS = "FETCH_STATUS_SUCCESS";
export const FETCH_STATUS_FAILURE = "FETCH_STATUS_FAILURE";

export const FETCH_CUSTOMERS_BEGIN = "FETCH_CUSTOMERS_BEGIN";
export const FETCH_CUSTOMERS_SUCCESS = "FETCH_CUSTOMERS_SUCCESS";
export const FETCH_CUSTOMERS_FAILURE = "FETCH_CUSTOMERS_FAILURE";

export const SAVE_UPDATE_CUSTOMER_BEGIN = "SAVE_UPDATE_CUSTOMER_BEGIN";
export const SAVE_UPDATE_CUSTOMER_SUCCESS = "SAVE_UPDATE_CUSTOMER_SUCCESS";
export const SAVE_UPDATE_CUSTOMER_FAILURE = "SAVE_UPDATE_CUSTOMER_FAILURE";

export const DELETE_CUSTOMER_BEGIN = "DELETE_CUSTOMER_BEGIN";
export const DELETE_CUSTOMER_SUCCESS = "DELETE_CUSTOMER_SUCCESS";
export const DELETE_CUSTOMER_FAILURE = "DELETE_CUSTOMER_FAILURE";

export const CLEAR_ERROR_MESSAGE = "CLEAR_ERROR_MESSAGE";

export const GET_CHARTDATA_BEGIN = "GET_CHARTDATA_BEGIN";
export const GET_CHARTDATA_SUCCESS = "GET_CHARTDATA_SUCCESS";
export const GET_CHARTDATA_FAILURE = "GET_CHARTDATA_FAILURE";
