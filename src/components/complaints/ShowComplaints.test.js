import React from "react";
import { render, unmountComponentAtNode } from "react-dom";
import { act } from "react-dom/test-utils";
import { Provider } from "react-redux";
import configureMockStore from "redux-mock-store";
import thunk from "redux-thunk";

import ShowCompliants from "./ShowComplaints";

describe("ShowCompliants testing", () => {
  let container = null;
  const middlewares = [thunk];
  const mockStore = configureMockStore(middlewares);

  beforeEach(() => {
    container = document.createElement("div");
    document.body.appendChild(container);
  });

  afterEach(() => {
    unmountComponentAtNode(container);
    container.remove();
    container = null;
  });

  it("render ShowCompliants loading component", () => {
    let store = mockStore({
      complaints: { entities: [], loading: true, error: null },
    });
    act(() => {
      render(
        <Provider store={store}>
          <ShowCompliants />
        </Provider>,
        container
      );
    });
    expect(container.textContent).toMatch(/Loading.../i);
  });

  it("render ShowCompliants Failed component", () => {
    let store = mockStore({
      complaints: {
        entities: [],
        loading: false,
        error: {
          message: "Failed to fetch complaints.. please try again later!!!",
        },
      },
    });
    act(() => {
      render(
        <Provider store={store}>
          <ShowCompliants />
        </Provider>,
        container
      );
    });
    expect(container.textContent).toMatch(/Failed/i);
  });

  it("render ShowCompliants Success component", () => {
    let store = mockStore({
      complaints: {
        entities: [
          {
            commentId: "42c8150e-6663-4649-a486-175da825a781",
            customerId: "4b1a32da-01d6-4f86-ae8a-8dd68b1d63c3",
            customerName: "Sudham P",
            title: "Title 2",
            description: "Desc 23",
            status: "OPEN",
            assignee: "0298b44d-b45a-47af-a09d-ecc9bebd88fb",
            assigneeName: "Mithun R",
          },
        ],
        loading: false,
        error: null,
      },
      customer: {
        entities: [],
      },
    });
    act(() => {
      render(
        <Provider store={store}>
          <ShowCompliants />
        </Provider>,
        container
      );
    });
    expect(container.textContent).toMatch(/Complaints/i);
  });
});
