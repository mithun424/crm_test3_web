import { Button, Modal, Form } from "react-bootstrap";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  SaveAndUpdateCustomer,
  fetchCustomers,
  clearErrorMeaage,
} from "../../actions/customerAction";
import Loader from "../common/Loader";
import { Formik } from "formik";

const emailRegex = RegExp(
  /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
);

const UpdateCustomer = (props) => {
  const dispatch = useDispatch();
  const [show, setShow] = useState(false);

  const handleClose = () => {
    setShow(false);
    dispatch(clearErrorMeaage());
  };
  const handleShow = () => {
    setShow(true);
  };

  const loading = useSelector((state) => state.customer.loading);
  const errorMessage = useSelector((state) => state.customer.errorMessage);

  return (
    <>
      <Button variant="outline-primary sm" onClick={handleShow}>
        {props.customer.id ? "Edit" : "Add Customer"}
      </Button>
      <Modal
        show={show}
        onHide={handleClose}
        backdrop="static"
        keyboard={false}
      >
        <Formik
          initialValues={Object.keys(props.customer).length ? props.customer : {firstName:'',lastName:'',email:'',mobile:'',role:'',address:''}}
          validate={(values) => {
            const errors = {};
            if (!values.firstName) {
              errors.firstName = "First name required";
            }
            if (!values.lastName) {
              errors.lastName = "Last name required";
            }
            if (!values.email) {
              errors.email = "Email Id required.";
            } else if (!emailRegex.test(values.email)) {
              errors.email = "Email address is invalid";
            }

            if (!values.mobile) {
              errors.mobile = "Mobile number required";
            }
            if (!values.address) {
              errors.address = "Address required";
            }
            if (!values.role) {
              errors.role = "Select valid role";
            }

            return errors;
          }}
          onSubmit={async (values, { setSubmitting }) => {
            const result = await dispatch(SaveAndUpdateCustomer(values));
            if (result) {
              dispatch(fetchCustomers());
            }
            setShow(false);
            setSubmitting(false);
          }}
        >
          {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
          }) => (
            <form onSubmit={handleSubmit} noValidate>
              <Modal.Header closeButton>
                <Modal.Title>
                  {props.customer.id ? "Edit Customer" : "Add Customer"}
                </Modal.Title>
              </Modal.Header>
              <Modal.Body>
                {errorMessage && (
                  <div className="alert alert-danger">
                    <strong>{errorMessage}</strong>
                  </div>
                )}
                <Form.Group controlId="formFirstName">
                  <Form.Label>First Name</Form.Label>
                  <input
                    className= {(errors.firstName && touched.firstName && errors.firstName) ? "is-invalid form-control" : "form-control"}
                    type="firstName"
                    name="firstName"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.firstName}
                  />
                  <span style={{ color: "#dc3545" }}>
                    {errors.firstName && touched.firstName && errors.firstName}
                  </span>
                </Form.Group>
                <Form.Group controlId="formLastName">
                  <Form.Label>Last Name</Form.Label>
                  <input
                   className= {(errors.lastName && touched.lastName && errors.lastName) ? "is-invalid form-control" : "form-control"}
                    type="lastName"
                    name="lastName"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.lastName}
                  />
                  <span style={{ color: "#dc3545" }}>
                    {errors.lastName && touched.lastName && errors.lastName}
                  </span>
                </Form.Group>
                <Form.Group controlId="formEmail">
                  <Form.Label>Email</Form.Label>
                  <input
                    className= {(errors.email && touched.email && errors.email) ? "is-invalid form-control" : "form-control"}
                    type="email"
                    name="email"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.email}
                  />
                  <span style={{ color: "#dc3545" }}>
                    {errors.email && touched.email && errors.email}
                  </span>
                </Form.Group>
                <Form.Group controlId="formMobile">
                  <Form.Label>Mobile</Form.Label>
                  <input
                    className= {(errors.mobile && touched.mobile && errors.mobile) ? "is-invalid form-control" : "form-control"}
                    type="mobile"
                    name="mobile"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.mobile}
                  />
                  <span style={{ color: "#dc3545" }}>
                    {errors.mobile && touched.mobile && errors.mobile}
                  </span>
                </Form.Group>
                <Form.Group controlId="formMobile">
                  <Form.Label>Address</Form.Label>
                  <input
                    className= {(errors.address && touched.address && errors.address) ? "is-invalid form-control" : "form-control"}
                    type="address"
                    name="address"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.address}
                  />
                  <span style={{ color: "#dc3545" }}>
                    {errors.address && touched.address && errors.address}
                  </span>
                </Form.Group>
                <Form.Group controlId="formRole">
                  <Form.Label>Role</Form.Label>
                  <select
                      className= {(errors.role && touched.role && errors.role) ? "is-invalid form-control" : "form-control"}
                    type="role"
                    name="role"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.role}
                  >
                    <option value="">SELECT</option>
                    <option value="ADMIN">ADMIN</option>
                    <option value="CUSTOMER">CUSTOMER</option>
                  </select>
                  <span style={{ color: "#dc3545" }}>
                    {errors.role && touched.role && errors.role}
                  </span>
                </Form.Group>
              </Modal.Body>
              <Modal.Footer>
                <Button variant="secondary" onClick={() => handleClose()}>
                  Close
                </Button>
                <Button variant="primary" type="submit" >  
                  {props.customer.id ? "Update" : "Save"}
                  {loading && <Loader />}
                </Button>
              </Modal.Footer>
            </form>
          )}
        </Formik>
      </Modal>
    </>
  );
};

export default UpdateCustomer;
