import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Navigationbar from "./Navigationbar";
import ShowCompliants from "./complaints/ShowComplaints";
import CustomerListing from "./customer/CustomerListing";
import Dashboard from "../components/Dashboard/Dashboard"

const App = () => {
  return (
    <Router>
      <main role="main">
        <Navigationbar />
        <br />
        <Switch>
          <Route exact path="/">
          <Dashboard/>
          </Route>
          <Route path="/customer">
            <CustomerListing />
          </Route>
          <Route path="/complaints">
            <ShowCompliants />
          </Route>
        </Switch>
      </main>
    </Router>
  );
};

export default App;
